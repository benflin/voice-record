const initialState = {
	isRecording: false,
}
export default function rootReducer(state = initialState, action) {
	switch(action.type) {
		case 'START_RECORDING':
			return { ...state, isRecording: true }
		case 'STOP_RECORDING':
			return { ...state, isRecording: false }
		default:
			return state
	}
}