const initialState = {
    recorder: null,
    recordings: null,
    activeRecording: 0,
    isRecording: false
}
export default function rootReducer(state = initialState, action) {
    console.log(state, action)
    switch(action.type) {
        case 'SET_RECORDER':
            return { ...state, recorder: action.recorder }
        case 'SET_RECORDINGS':
            return { ...state, recordings: action.recordings }
        case 'ACTIVE_RECORDING':
            return { ...state, activeRecording: action.index }
        case 'START_RECORDING':
            return { ...state, isRecording: true }
        case 'STOP_RECORDING':
            return { ...state, isRecording: false }
        default:
            return state
    }
}