import io from 'socket.io-client'
const socket = io(':3001')

export const startRecording = () => dispatch => {
	socket.emit('start-recording')

	dispatch({
		type: 'START_RECORDING'
	})
}

export const stopRecording = () => dispatch => {
	socket.emit('stop-recording')

	dispatch({
		type: 'STOP_RECORDING'
	})
}
export const nextRecording = () => dispatch => {
	socket.emit('next-recording')
}
export const prevRecording = () => dispatch => {
	socket.emit('prev-recording')
}