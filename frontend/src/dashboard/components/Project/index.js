import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import UploadCSV from '../UploadCSV'
import Edit from '../Edit'
import DownloadAll from '../DownloadAll'
import Controller from '../Controller'
export default class Project extends React.Component {

    constructor(props) {
        super(props)
    }


    render() {
        return (
            <div className='d-flex align-items-center border m-2 rounded'>
                <i className="fas fa-angle-right p-3"></i>
                <a href={`/recorder.html?name=${this.props.project.name}`}><h3>{ this.props.project.name }</h3></a>
                <div className='ml-auto d-flex flex-column align-items-center text-center pl-2 border-left p-2'>
                    <DownloadAll project={ this.props.project }/>
                    <UploadCSV project={ this.props.project }/>
                    <Controller project={ this.props.project}/>
                    <Edit project={ this.props.project }/>
                </div>
            </div>
        )
    }
}