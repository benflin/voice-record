import React from 'react'
import Audio from '../Audio'
import Caption from '../Caption'
import './style.css'

export default class Recording extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className={`recording border rounded m-2 d-flex flex-column ${this.props.active ? 'bg-dark text-white' : ''}`}>
                <div className='d-flex border-bottom'>
                    <div className='filename'>
                        { this.props.name }
                    </div>
                    <Audio name={ this.props.name } buffer={ this.props.buffer }/>
                </div>
                <Caption caption={ this.props.caption } visible={ this.props.active }/>
            </div>
        )
    }
}