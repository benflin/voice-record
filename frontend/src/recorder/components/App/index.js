import React from 'react'
import Navigation from '../Navigation'
import RecordingList from '../RecordingList'
import RecordingControls from '../RecordingControls'
import 'bootstrap/dist/css/bootstrap.min.css'
import { connect } from 'react-redux'
import { setupAudioRecorder, getRecordingData, startSocket } from '../../actions'
class App extends React.Component {

    constructor(props) {
        super(props)
        props.setupAudioRecorder()
        props.getRecordingData()
        props.startSocket()
    }

    render() {
        return (
            <div>
                <Navigation />
                <div className='p-3'>
                    <div className='d-flex flex-column p-4'>
                        <RecordingControls />
                        <div className='p-3'>
                            <RecordingList />
                        </div>
                        <div className='p-3'>
                            
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    
})
const mapDispatchToProps = dispatch => ({
    setupAudioRecorder: () => dispatch(setupAudioRecorder()),
    getRecordingData: () => dispatch(getRecordingData()),
    startSocket: () => dispatch(startSocket())
})
export default connect(mapStateToProps, mapDispatchToProps)(App)