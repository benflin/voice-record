const User = require('../models/user')

module.exports = (req, res, next) => {

    try {
        User.findOne({ profileId: req.session.passport.user.profileId, sessionId: req.sessionID })
            .exec((err, user) => {
                if(err) {
                    throw new Error(err)
                } else {
                    req.user = user
                    next()
                }
            })
    } catch(err) {
        console.log(err)
        res.redirect('/')
    }
}