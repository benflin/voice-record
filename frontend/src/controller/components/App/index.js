import React from 'react'
import { connect } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'
import { startRecording, stopRecording, nextRecording, prevRecording } from '../../actions'
class App extends React.Component {

    toggleRecord() {
        if(!this.props.isRecording) {
            this.props.startRecording()
        } else {
            this.props.stopRecording()
        }
    }
    nextRecording() {
        if(this.props.isRecording)
            this.props.stopRecording()
        this.props.nextRecording()
    }

    prevRecording() {
        if(this.props.isRecording)
            this.props.stopRecording()
        this.props.prevRecording()
    }

    render() {

        const buttonActive = this.props.isRecording ? 'btn-record-active' : ''
        
        return (
            <div className='App d-flex flex-column justify-content-center align-items-center'>
                <button className={`btn btn-danger m-2 rounded-circle btn-record ${buttonActive}`} onClick={ this.toggleRecord.bind(this) }>
                    <i className="fas fa-dot-circle"></i>
                </button>
                <div className='btn-group'>
                    <button className="btn btn-lg btn-light" onClick={ this.prevRecording.bind(this) }>
                        <i className="fas fa-chevron-left"></i>
                        <span className='ml-2'>Prev</span>
                    </button>
                    <button className="btn btn-lg btn-light" onClick={ this.nextRecording.bind(this) }>
                        <span className='mr-2'>Next</span>
                        <i className="fas fa-chevron-right"></i>
                    </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ isRecording }) => ({ isRecording })
const mapDispatchToProps = dispatch =>({
    startRecording: () => dispatch(startRecording()),
    stopRecording: () => dispatch(stopRecording()),
    nextRecording: () => dispatch(nextRecording()),
    prevRecording: () => dispatch(prevRecording())
})
export default connect(mapStateToProps, mapDispatchToProps)(App)
