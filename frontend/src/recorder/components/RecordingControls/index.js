import React from 'react'
import { connect } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css'
import { nextRecording, prevRecording, startRecording, stopRecording, download } from '../../actions'
import Timer from 'timer.js'

class RecordingControls extends React.Component {

    constructor(props) {
        super(props)


        this.state = { time: 0, disabled: true, popup: false }

        this.timer = new Timer()
        this.timer.on('tick', ms => {
            this.setState({ time: Math.round(ms / 1000) })
        })
        this.timer.on('end', () => {
            if(!this.state.disabled) {
                this.props.startRecording()
                this.setState({ popup: true })
                setTimeout(() => this.setState({ popup: true }), 1000)
            }
        })

    }

    next() {
        this.props.nextRecording()
        if(this.props.isRecording)
            this.props.stopRecording()
        if(!this.state.disabled)
            this.startTimer()
    }

    prev() {
        this.props.prevRecording()
        if(this.props.isRecording)
            this.props.stopRecording()
        if(!this.state.disabled)
            this.startTimer()
    }

    startTimer() {
        if(!this.state.disabled) {
            this.timer.start(4)
            this.setState({ popup: false })
        }
    }
    stopTimer() {
        this.timer.stop()
        this.setState({ popup: false })
    }
    download() {
        this.props.download()
    }

    toggleDisableTimer() {
        this.setState({ disabled: !this.state.disabled })
    }

    render() {
        return (
            <div className='btn-group'>
                <button className="btn btn-success"  onClick={ this.startTimer.bind(this) }>
                    Start Timer
                </button>
                <button className="btn btn-danger" onClick={ this.stopTimer.bind(this) }>
                    Stop Timer
                </button>
                <button className="btn btn-secondary" onClick={ this.toggleDisableTimer.bind(this) }>
                    { this.state.disabled ? 'Enable Timer' : 'Disable Timer'}
                </button>
                <button className="btn btn-light" onClick={ this.prev.bind(this) }>
                    <i className="fas fa-chevron-left"></i>
                    <span className='ml-2'>Prev</span>
                </button>
                <button className="btn btn-light" onClick={ this.next.bind(this) }>
                    <span className='mr-2'>Next</span>
                    <i className="fas fa-chevron-right"></i>
                </button>
                <button className="btn btn-primary" onClick={ this.download.bind(this) }>
                    <span className='mr-2'>Download</span>
                    <i className="fas fa-download"></i>
                </button>
                <button className="btn btn-light">
                    Time: { this.state.time }s
                </button>
                { this.state.popup && 
                    <div className='fixed-bottom text-center bg-success card-body text-white'>
                        <h1>GO!</h1>
                    </div>
                }
            </div>
        )
    }

}

const mapStateToProps = state => ({
    activeRecording: state.activeRecording,
    recordings: state.recordings,
    isRecording: state.isRecording
})
const mapDispatchToProps = dispatch => ({
    prevRecording: () => dispatch(prevRecording()),
    nextRecording: () => dispatch(nextRecording()),
    startRecording: () => dispatch(startRecording()),
    stopRecording: () => dispatch(stopRecording()),
    download: () => dispatch(download())
})
export default connect(mapStateToProps, mapDispatchToProps)(RecordingControls)