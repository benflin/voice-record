const express = require('express')
const app = express()
const path = require('path')
const passport = require('passport')
const session = require('express-session')
const dashboardRouter = require('./api/dashboard')
const recordingRouter = require('./api/recording')
const controllerRouter = require('./api/controller')
const passportRouter = require('./passport')(passport)
const { sessionSecret, keyPath, certPath } = require('./secret.js')
const sharedsession = require('express-socket.io-session')
const isLoggedInSocket = require('./util/logged-in-socket')
const https = require('https')
const fs = require('fs')
const privateKey  = fs.readFileSync(keyPath, 'utf8');
const certificate = fs.readFileSync(certPath, 'utf8');
const socketServer = https.createServer({key: privateKey, cert: certificate})
const io = require('socket.io')(socketServer)
const httpPort = process.env.HTTP_PORT || 80
const httpsPort = process.env.HTTPS_PORT || 443

const sess = session({
    secret: sessionSecret,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
})
app.use(sess)
socketServer.listen(3001)

app.use(passport.initialize())
app.use(passport.session())
app.use(passportRouter)

app.use(recordingRouter)
app.use(dashboardRouter)
app.use(controllerRouter)

io.use(sharedsession(sess, {
    autoSave: true
}))

io.use(isLoggedInSocket)

io.on('connection', socket => {

    const room = socket.handshake.session.passport.user.profileId
    socket.join(room)

    socket.on('start-recording', () => io.to(room).emit('start-recording'))
    socket.on('stop-recording', () => io.to(room).emit('stop-recording'))
    socket.on('prev-recording', () => io.to(room).emit('prev-recording'))
    socket.on('next-recording', () => io.to(room).emit('next-recording'))
})


app.get('/', (req, res) => {
    res.redirect('/auth/google')
})
const httpsServer = https.createServer({key: privateKey, cert: certificate}, app)
httpsServer.listen(httpsPort)
const httpServer = express()

httpServer.get('*', function(req, res) {  
    res.redirect('https://' + req.headers.host + req.url)
})
httpServer.listen(httpPort)