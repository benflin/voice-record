const User = require('../models/user')
module.exports = (socket, next) => {

    try {

        User.findOne({ 
                profileId: socket.handshake.session.passport.user.profileId,
                sessionId: socket.handshake.sessionID
            })
            .exec((err, user) => {
                if(err) {
                    console.error(err)
                } else {
                    next()
                }
        })
    
    } catch(err) {
        console.error(err)
        return
    }
}