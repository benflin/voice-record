import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { connect } from 'react-redux'
import { uploadCSV } from '../../actions'
class UploadCSV extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            disabled: this.props.project.csvUploaded
        }
    }

    triggerFileChooser() {
        if(this.state.disabled) return
        this.refs.input.click()
    }

    uploadCSV(event) {
        this.props.uploadCSV(this.props.project.name, event.target.files[0])
    }

    render() {
        return (
            <button className='btn btn-warning btn-sm text-white' onClick={ this.triggerFileChooser.bind(this) } disabled={ this.state.disabled }>
                <span className='mr-2'>Upload CSV</span>
                <input type='file' ref='input' style={{display: 'none'}} onChange={ this.uploadCSV.bind(this) }/>
                <i className="fas fa-upload"></i>
            </button>
        )
    }
}

const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => ({
    uploadCSV: (name, file) => dispatch(uploadCSV(name, file)),
})
export default connect(mapStateToProps, mapDispatchToProps)(UploadCSV)