const express = require('express')
const path = require('path')
const os = require('os')

const bodyParser = require('body-parser')
const uuid = require('uuid/v4')
const multer = require('multer')
const User = require('../models/user')
const isLoggedIn = require('../util/logged-in-middleware')
const frontendPath = path.join(__dirname, '../../frontend/dist/')
const csv = require('csv-parser')
const fs = require('fs')
const router = express.Router()
const upload = multer({ dest: os.tmpdir() })

router.get('/api/dashboard/projects', (req, res) => {
    
    res.json(req.user.getProjects())

})

router.post('/api/dashboard/upload-csv', upload.single('csv'), (req, res) => {
    
    const audios = []
    fs.createReadStream(req.file.path)
        .pipe(csv(['name', 'caption']))
        .on('data', audio => audios.push(audio))
        .on('end', () => {
            req.user.setAudios(req.body.name, audios)
                .then(({ projects }) => res.json(projects))
        })

})

router.post('/api/dashboard/create-project', bodyParser.json(), (req, res) => {
    
    req.user.createProject(req.body.name)
        .then(({ projects }) => res.json(projects))

})

router.post('/api/dashboard/rename-project', bodyParser.json(), (req, res) => {

    req.user.renameProject(req.body.oldName, req.body.newName)
        .then(({ projects }) => res.json(projects))

})

router.post('/api/dashboard/delete-project', bodyParser.json(), (req, res) => {

    req.user.deleteProject(req.body.name)
        .then(({ projects }) => res.json(projects))
})

router.get(/dashboard/, (req, res) => {
    console.log(req.user)
    res.sendFile(path.join(frontendPath, req.path))
})


module.exports = router