const mongoose = require('mongoose')
const findOrCreate = require('mongoose-find-or-create')
const AudioSchema = new mongoose.Schema({
    name: String,
    caption: String,
    buffer: Buffer
})

const ProjectSchema = new mongoose.Schema({
    name: { type: String, index: { unique: true } },
    audios: [ AudioSchema ],
    csvUploaded: Boolean
})
ProjectSchema.plugin(findOrCreate)

const UserSchema = new mongoose.Schema({
    profileId: { type: String, index: { unique: true } },
    accessToken: String,
    refreshToken: String,
    sessionId: String,
    projects: [ ProjectSchema ]
})

UserSchema.methods.getProjects = function() {
    return this.projects
}

UserSchema.methods.createProject = function(name) {
    
    const project = this.projects.find(project => project.name == name)
    
    if(!project)
        this.projects.push({ name })
    
    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else   
                resolve(user)
        })
    })
}
UserSchema.methods.deleteProject = function(name) {

    const projectIndex = this.projects.findIndex(project => project.name == name)
    this.projects.splice(projectIndex, 1)
    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else   
                resolve(user)
        })
    })

}

UserSchema.methods.renameProject = function(oldName, newName) {

    const project = this.projects.find(project => project.name == oldName)
    project.name = newName
    
    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else   
                resolve(user)
        })
    })

}
UserSchema.methods.addAudio = function(projectName, audioName, audioBuffer) {
    const project = this.projects.find(project => project.name == projectName)
    const audio = project.audios.find(audio => audio.name == audioName)
    audio.buffer = audioBuffer

    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else {   
                resolve(user)
            }
        })
    })
}


UserSchema.methods.setAudios = function(projectName, audios) {

    const project = this.projects.find(project => project.name == projectName)

    project.audios = audios

    project.csvUploaded = true

    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else {   
                resolve(user)
            }
        })
    })
    
}

UserSchema.methods.getAudios = function(projectName) {

    const project = this.projects.find(project => project.name == projectName)
    return project.audios
    
}

UserSchema.methods.register = function(profileId, accessToken, refreshToken) {

    this.profileId = profileId
    this.accessToken = accessToken
    this.refreshToken = refreshToken
    
    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else {   
                resolve(user)
            }
        })
    })
}

UserSchema.methods.setSessionId = function(sessionId) {

    this.sessionId = sessionId

    return new Promise((resolve, reject) => {
        this.save((err, user) => {
            if(err)
                reject(err)
            else   
                resolve(user)
        })
    })
}

UserSchema.methods.findProject = function(name) {
    return this.projects.find(project => project.name === name)
}
UserSchema.plugin(findOrCreate)
mongoose.connect('mongodb://localhost:27017/users')
const db = mongoose.connection
db.on('error', console.error)
db.once('open', console.log)

module.exports = mongoose.model('User', UserSchema)
