const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: "html-loader"
                }]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    entry: {
        recorder: './src/recorder',
        dashboard: './src/dashboard',
        controller: './src/controller'
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: './src/recorder/index.html',
            chunks: ['recorder'],
            filename: './recorder.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/dashboard/index.html',
            chunks: ['dashboard'],
            filename: './dashboard.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/controller/index.html',
            chunks: ['controller'],
            filename: './controller.html'
        })
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};