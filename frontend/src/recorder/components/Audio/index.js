import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'
import { connect } from 'react-redux'
import { startRecording, stopRecording } from '../../actions'

class Audio extends React.Component {

    constructor(props) {
        super(props)
    }

    setSrc() {
        if(this.refs.audio && this.props.buffer) {
            this.refs.audio.src = URL.createObjectURL(new Blob([new Uint8Array(this.props.buffer.data)], { 'type' : 'audio/webm' }))
        } else if(this.refs.audio) {
            this.refs.audio.src = ''
        }
    }

    componentDidMount() {
        this.setSrc()
    }

    componentDidUpdate() {
        this.setSrc()
        if(!this.props.isRecording)
            this.refs.audio.play()
    }
  
    onRecord() {
        if(!this.props.isRecording) {
            this.props.startRecording()
        } else if(this.props.isRecording) {
            this.props.stopRecording()
        }
    }

    render() {

        const buttonActive = this.props.isRecording ? 'btn-record-active' : ''
        return (
            <div className={`ml-auto d-flex align-items-center`}>
                <button className={`btn btn-danger m-2 rounded-circle btn-record ${buttonActive}`} onClick={ this.onRecord.bind(this) }>
                    <i className="fas fa-dot-circle"></i>
                </button>
                <audio ref='audio' src='' controls/>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    isRecording: state.isRecording
})

const mapDispatchToProps = dispatch => ({
    startRecording: () => dispatch(startRecording()),
    stopRecording: () => dispatch(stopRecording())
})

export default connect(mapStateToProps, mapDispatchToProps)(Audio)