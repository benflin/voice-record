const express = require('express')
const path = require('path')
const os = require('os')
const fs = require('fs')
const bodyParser = require('body-parser')
const multer = require('multer')
const isLoggedIn = require('../util/logged-in-middleware')
const frontendPath = path.join(__dirname, '../../frontend/dist/')
const archiver = require('archiver')
const router = express.Router()
const storage = multer.memoryStorage()
const upload = multer({ storage })
const uuid = require('uuid/v4')


router.get(/recorder/, (req, res) => {
    res.sendFile(path.join(frontendPath, req.path))
})

router.get('/api/recording/data', (req, res) => {

    res.json(req.user.getAudios(req.query.name))

})

router.post('/api/recording/post', upload.single('audio'), (req, res) => {

    req.user.addAudio(req.query.name, req.body.name, req.file.buffer)
        .then(() => res.json(req.user.getAudios(req.query.name)))
    
})

router.get('/api/recording/download', (req, res) => {
    const project = req.user.findProject(req.query.name)
    console.log(req.query.name)
    const archive = archiver('zip', {
        zlib: { level: 9 }
    })
    
    console.log('project: ', project)
    const pathOutput = path.join(os.tmpdir(), uuid() + '.zip')
    const output = fs.createWriteStream(pathOutput)
    output.on('close', () => {
        console.log('end: ', pathOutput)
        res.sendFile(pathOutput)
    })
    archive.pipe(output)
    project.audios
        .forEach(audio => {
            if(audio.buffer)
                archive.append(Buffer.from(audio.buffer), { name: audio.name + '.webm' })}
        )
    archive.finalize()

})



module.exports = router