import audioRecorder from '../util/audio-recorder'
import getQuery from '../util/query'
import io from 'socket.io-client'
const socket = io(':3001')
export const startSocket = () => dispatch => {
    socket.on('start-recording', () => dispatch(startRecording()))
    socket.on('stop-recording', () => dispatch(stopRecording()))
    socket.on('prev-recording', () => dispatch(prevRecording()))
    socket.on('next-recording', () => dispatch(nextRecording()))
}

export const nextRecording = () => (dispatch, getState) => {
    const { activeRecording, recordings } = getState()
    const newActiveRecording = Math.min(Math.max(activeRecording + 1, 0), recordings.length)
    dispatch(setActiveRecording(newActiveRecording))
}

export const prevRecording = () => (dispatch, getState) => {
    const { activeRecording, recordings } = getState()
    const newActiveRecording = Math.min(Math.max(activeRecording - 1, 0), recordings.length)
    dispatch(setActiveRecording(newActiveRecording))
}

export const addRecording = recording => ({
    type: 'ADD_RECORDING',
    recording
})

export const setAudioRecorder = recorder => ({
    type: 'SET_RECORDER',
    recorder
})
export const setRecordings = recordings => ({
    type: 'SET_RECORDINGS',
    recordings
})
export const getRecordingData = () => dispatch => {
    fetch(`/api/recording/data?name=${getQuery('name')}`)
        .then(res => res.json())
        .then(recordings => dispatch(setRecordings(recordings)))

}

export const postRecordingFile = (file, name) => dispatch => {
    const formData = new FormData()
    formData.append('audio', file)
    formData.append('name', name)
    return fetch(`/api/recording/post?name=${getQuery('name')}`, {
        method: 'POST',
        body: formData
    })
    .then(res => res.json())
    .then(recordings => dispatch(setRecordings(recordings)))

}
export const setupAudioRecorder = () => dispatch => {
    audioRecorder().then(recorder => dispatch(setAudioRecorder(recorder)))
}

export const setActiveRecording = index => ({
    type: 'ACTIVE_RECORDING',
    index
})
export const startRecording = () => (dispatch, getState) => {
    dispatch({
        type: 'START_RECORDING'
    })
    const { recorder } = getState()
    recorder.start()
}

export const stopRecording = () => (dispatch, getState) => {

    const { recorder, activeRecording, recordings } = getState()
    const name = recordings[activeRecording].name
    recorder.stop(blob => {
        dispatch(postRecordingFile(blob, name))
    })
    dispatch({
        type: 'STOP_RECORDING'
    })

}
export const download = () => {
    window.location = `/api/recording/download?name=${getQuery('name')}`
}

