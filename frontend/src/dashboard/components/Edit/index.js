import React from 'react' 
import $ from 'jquery'
import { deleteProject, renameProject } from '../../actions'
import { connect } from 'react-redux'
class Edit extends React.Component {


    popModal() {
        $(this.refs.modal).modal()
    }

    popDeleteModal() {
        $(this.refs.delete).modal()
    }

    deleteProject() {
        $(this.refs.delete).modal('toggle')
        $(this.refs.modal).modal('toggle')
        this.props.deleteProject(this.props.project.name)
    }

    renameProject() {
        $(this.refs.modal).modal('toggle')
        this.props.renameProject(this.props.project.name, this.refs.input.value)
    }

    render() {

        return (
            <React.Fragment>
                <a href='#' onClick={ this.popModal.bind(this) }>
                    <span className='mr-2'>Edit</span>
                    <i className="fas fa-edit"></i>
                </a>
                <div ref='modal' className="modal" tabIndex={-1} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Edit Project</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroup-sizing-default">Name</span>
                        </div>
                            <input ref='input' placeholder='new_project' type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" onClick={ this.renameProject.bind(this) }>Save changes</button>
                        <button type="button" className="btn btn-danger" onClick={ this.popDeleteModal.bind(this) }>
                            Delete Project
                        </button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
                <div ref='delete' className="modal" tabIndex={-1} role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Delete Project</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                        <h5>Are you sure you want to delete this project? (This cannot be undone)</h5>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger" onClick={ this.deleteProject.bind(this) }>Delete</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </React.Fragment>

        )

    }

}

const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => ({
    deleteProject: id => dispatch(deleteProject(id)),
    renameProject: (id, name) => dispatch(renameProject(id, name))
})
export default connect(mapStateToProps, mapDispatchToProps)(Edit)