export default function audioRecorder(onData) {

	const create = stream => {

		const ctx = new AudioContext()
		const source = ctx.createMediaStreamSource(stream)
		const dest = ctx.createMediaStreamDestination()
		const mediaRecorder = new MediaRecorder(dest.stream)

		let stopCallback = () => {}

		source.connect(dest)

		let chunks = []


		mediaRecorder.ondataavailable = evt => {
			
			if(onData)
				onData(evt.data)

			chunks.push(evt.data)

		}


		mediaRecorder.onstop = () => {
			const blob = new Blob(chunks, { 'type' : 'audio/webm' })
			stopCallback(blob, chunks) 
		}

		const start = () => {
			chunks = []
			mediaRecorder.start()
		}
		
		const stop = (callback) => {
			mediaRecorder.stop()
			callback ? stopCallback = callback : null
		}

		return { start, stop, mediaRecorder, stream }
	}

	return navigator.mediaDevices.getUserMedia({ audio: true })
		.then(create)
}