import React from 'react'
import { connect } from 'react-redux'
import { createProject, getProjects } from '../../actions'
import $ from 'jquery'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'

class CreateProject extends React.Component {

    constructor(props) {
        super(props)
    }

    popModal() {
        $(this.refs.modal).modal()
    }

    createProject() {
        const name = this.refs.input.value
        $(this.refs.modal).modal('hide')
        this.props.createProject({ name })
        this.props.getProjects()
    }

    render() {
        return (
            <React.Fragment>
            <div className='d-flex align-items-center border border-dashed m-2 py-4 rounded justify-content-center'>
                <a href='#' onClick={ this.popModal.bind(this) } className='d-flex text-center align-items-center p-2'>
                    <h3 className='p-3'> Create Project </h3>
                    <h3 className="fas fa-plus"></h3>
                </a>
            </div>
            <div ref='modal' className="modal" tabIndex={-1} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Create Project</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroup-sizing-default">Name</span>
                        </div>
                            <input ref='input' placeholder='new_project' type="text" className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" onClick={ this.createProject.bind(this) }>Create</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
            </div>

            </React.Fragment>
        )
    }
}
const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => ({
    createProject: name => dispatch(createProject(name)),
    getProjects: () => dispatch(getProjects())
})
export default connect(mapStateToProps, mapDispatchToProps)(CreateProject)