import React from 'react'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'

export default class Navigation extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ul className='nav'>
                <li className='nav-item'>
                    <a className='nav-link' href='#'>
                        <span className='mx-2'>Dashboard</span> 
                        <i className='fas fa-tachometer-alt'></i>
                    </a>
                </li>
                <li className='nav-item dropdown ml-auto'>
                    <a className='nav-link dropdown-toggle' href='#' data-toggle='dropdown'>
                        Account
                    </a>
                    <div className='dropdown-menu'>
                        <a className='dropdown-item' href='#'>Manage</a>
                        <a className='dropdown-item' href='#'>Sign out</a>
                    </div>
                </li>
            </ul>
        )
    }
}