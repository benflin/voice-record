const clearProjects = projects => ({
    type: 'CLEAR_PROJECTS'
})
const addProject = project => ({
    type: 'ADD_PROJECT',
    project: project
})



export const createProject = project => dispatch => {
    fetch('/api/dashboard/create-project', {
        method: 'POST',
        body: JSON.stringify(project),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(projects => {
        dispatch(clearProjects())
        projects.map(project => dispatch(addProject(project)))
    })
}

export const getProjects = () => dispatch => {
    dispatch(clearProjects())
    fetch('/api/dashboard/projects')
        .then(res => res.json())
        .then(projects => projects.map(project => {
            dispatch(addProject(project))
        }))
}

export const renameProject = (oldName, newName) => dispatch => {
    fetch('/api/dashboard/rename-project', {
        method: 'POST',
        body: JSON.stringify({ oldName, newName }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(projects => {
        dispatch(clearProjects())
        projects.map(project => dispatch(addProject(project)))
    })
}
export const deleteProject = name => dispatch => {
    fetch('/api/dashboard/delete-project', {
        method: 'POST',
        body: JSON.stringify({ name }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(projects => {
        dispatch(clearProjects()) 
        projects.map(project => dispatch(addProject(project)))
    })
}

export const uploadCSV = (name, csv) => dispatch => {
    const formData = new FormData();
    formData.append('name', name)
    formData.append('csv', csv)
    fetch('/api/dashboard/upload-csv', {
        method: 'POST',
        body: formData
    })
    .then(res => res.json())
    .then(projects => {
        dispatch(clearProjects()) 
        projects.map(project => dispatch(addProject(project)))
    })
}
