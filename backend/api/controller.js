const express = require('express')
const router = express.Router()
const path = require('path')
const frontendPath = path.join(__dirname, '../../frontend/dist/')

router.get(/controller/, (req, res) => {
    res.sendFile(path.join(frontendPath, req.path))
})

module.exports = router