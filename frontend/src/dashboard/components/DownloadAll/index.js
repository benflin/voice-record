import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

export default class DownloadAll extends React.Component {


    download() {
        window.location = `/api/recording/download?name=${this.props.project.name}`
    }

    render() {

        return (
            <button className='btn btn-primary btn-sm mb-1' onClick={this.download.bind(this)}>
                <span className='mr-2'>Download All</span>
                <i className="fas fa-download"></i>
            </button>
        )
    }
}