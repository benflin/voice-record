# Voice Recording Tool #

This project was built on top of a MERN stack (MongoDB, Express, React, Node).

The app structure looks like this:

```
app
|- backend
    |- api 
        controller.js (will be removed)
        dashboard.js
        recording.js
    |- models 
        user.js
    |- util
        logged-in-middleware.js (will be removed)
        logged-in-socket.js (will be removed)
    index.js
    passport.js
    server.crt
    server.key
    secret.js
    package.json
    package-lock.json
|- frontend
    |- dist
        |- ... webpack bundled items
    |- src
        |- account (not implemented yet)
        |- common
        |- controller (will be removed)
        |- dashboard
        |- recorder
    .babelrc
    webpack.config.js
    package.json
    package-lock.json
```

Each subdirectory of app is its own npm project, meaning they have a `package.json` with dependencies.

# Structure #

## `backend/` ##

The backend was built using Mongoose and Express

#### `index.js` ####
The main `express` server. It attaches all of the router in `api/` and handles the socket.io calls. *(Note: the controller page/routes will be deleted in the next update and therefore socket.io will not be needed)*

#### `passport.js` ####
Handles all of the Google OAuth authentication using the `passport` node module.

#### `secret.js` ####
Contains sensitive configuration information such as the `sessionSecret` and `clientSecret`. *(Note: At the moment, the callbacks, secrets and clientIds are authorized by google on a project on `ben@flin.org` google account. You will probably want to change this to your own account.)*

#### `server.crt` & `server.key` ####
Example self-signed SSL certificate and key.

#### `api/` ####
This subdirectory of `backend/` handles all of the routes for each of the pages and exports an instance of `express.Router()`. These are used in index.js

#### `models/user.js` ####
Contains the `mongoose` schema for each user. General interface for connecting to mongo server.

#### `util/` ####
Not used - will be removed in next update.


## `frontend/` ##

Most of the frontend was built using React + Redux. The Redux Thunk middleware is used for `fetch()` calls.

### `src/<page>` ###

Each page corresponds to a route in `backend/api/` 

##### `components/` #####
Contains all of the `react` ui components. Some will be wrapped in `connect()` which will allow it to access global state.

##### `actions/` #####
Contains all of the action methods and side-effects (such as `fetch()` calls to the server).

##### `reducer/` #####
Reduces and updates global state based on a dispatched action.

##### `util/` #####
Utilities which do not fit in any of the above categories.


# How to run for develpment #

`cd` to `frontend/` and type 
```
npm run watch
```
Then `cd` to `backend/` and type
```
npm start
```
or type
```
nodemon index.js
```
if you want the server to automatically restart due to changes.

*(Note: you may need to run `npm install` in each subdirectory).*

# How to run for production #

`cd` to `frontend/` and type 
```
npm run build
```
Then `cd` to `backend/` and type
```
nohup sudo npm start &
```

*(Note: you may need to run `npm install` in each subdirectory). You may also need to change the port in `index.js` as likely your local machine will not allow you to run on port 80/443)*

# Amazon Lightsail Instance #
The static ip is:
`34.221.64.244`
and the user is `bitnami`

You will need a SSH key from Brian to login.

There should be a `mongod` process running on the server. If its not running, type
```
cd ~
nohup mongod dbpath=data/ &
```
The actual web-app directory is in `node/voice-record` and the mongo dbpath is at `data/`.


# Deployment #
As of yet there is no real deployment process besides cloning this project and updating `secret.js`. 