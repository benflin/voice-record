import React from 'react'

export default class Caption extends React.Component {
    
    render() {
        return (
            <div className='text-body'>
                <div className="card-body">
                    { this.props.caption }
                </div>
            </div>
        )
    }
}