import React from 'react'
import Navigation from '../Navigation'
import Project from '../Project'
import CreateProject from '../CreateProject'
import { getProjects } from '../../actions'
import { connect } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css'
import uuid from 'uuid/v4'

class App extends React.Component {

    constructor(props) {
        super(props)
        props.getProjects()
    }

    render() {
        
        const projectComponents = this.props.projects
            .map(project => <Project project={project} key={uuid()} />)
        
        return (
            <div>
                <Navigation />
                <div className='p-5'>
                    { projectComponents }
                    <CreateProject />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        projects: state.projects
    }
}
const mapDispatchToProps = dispatch => ({
    getProjects: () => dispatch(getProjects())
})
export default connect(mapStateToProps, mapDispatchToProps)(App)