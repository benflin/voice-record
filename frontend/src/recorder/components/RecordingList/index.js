import React from 'react'
import Recording from '../Recording'
import { connect } from 'react-redux'
class RecordingList extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const renderComponents = this.props.recordings
            ? this.props.recordings
                .filter((_, index) => index == this.props.activeRecording)
                .map((recording, index) => <Recording name={ recording.name } caption={ recording.caption } buffer={ recording.buffer } />)
            : <div> No CSV Uploaded </div>

        return (
            <div>
                { renderComponents }
            </div>
        )
    }
}
const mapStateToProps = state => ({
    recordings: state.recordings,
    activeRecording: state.activeRecording
})
const mapDispatchToProps = () => ({})
export default connect(mapStateToProps, mapDispatchToProps)(RecordingList)