const express = require('express')
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
const User = require('./models/user')
const { clientID, clientSecret } = require('./secret.js')

module.exports = passport => {

    const router = express.Router()

    passport.use(
        new GoogleStrategy({
                clientID,
                clientSecret,
                callbackURL: '/auth/google/callback'
            },
            (accessToken, refreshToken, profile, done) => {
    
                User.findOrCreate({ profileId: profile.id }, (err, user) => {
                    if(err)
                        console.error(err)
                    user.profileId = profile.id
                    user.accessToken = accessToken
                    user.refreshToken = refreshToken
                    user.save((err) => {
                        
                        return done(err, user)
                    })
                })
    
            }
    ))
    
    
    router.get('/auth/google',
        passport.authenticate('google', {
            scope: ['https://www.googleapis.com/auth/plus.login']
        })
    )
    
    router.get('/auth/google/callback',
        passport.authenticate('google', {
            failureRedirect: '/auth/google'
        }),
    
        (req, res, next) => {

            res.redirect('/dashboard.html')
        }
    )
    
    passport.serializeUser(function (user, done) {

        done(null, user.profileId);
    })
    
    passport.deserializeUser(function (profileId, done) {
        
        User.findOne({ profileId }, (err, user) => {
            console.log(user)
            done(err, user)
        })
    })

    return router

}