const initialState = {
    projects: []
}
export default function rootReducer(state = initialState, action) {
    switch(action.type) {
        case 'CLEAR_PROJECTS':
            return initialState
        case 'ADD_PROJECT':
            return {
                projects: [
                    ...state.projects,
                    action.project
                ]
            }
        default:
            return state
    }
}